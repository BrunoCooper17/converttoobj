Small script that generates OBJ meshes from json files created with ![TexturePacker](https://www.codeandweb.com/texturepacker) (spritesheet generator).

The reason behind this script it's because since unreal engine's Paper2D sprites didn't work well for our future game. So, instated of using Paper2D, we use 3D Meshes (it renders faster).

The Script uses python 3.6 and in texturepacker we need to export to JSON (Array).

Programmed by:
 - Jesús Mastache Caballero (BrunoCooper17)

 [Portafolio](https://brunocooper17.gitlab.io/portafolio/projects)
 [Twitter](https://twitter.com/BrunoCooper_17)
 [LinkedIn](https://www.linkedin.com/in/jesus-mastache-25265952/)
