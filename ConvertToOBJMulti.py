#!/usr/bin/env python3.6

import sys
import json

from tkinter import filedialog
from tkinter import *

def RotateVertexXY(Vertex):
    tmpValue = Vertex[0]
    Vertex[0] = -Vertex[2]
    Vertex[2] = tmpValue
    return

def RotateVertexXZ(Vertex):
    tmpValue = Vertex[0]
    Vertex[0] = -Vertex[1]
    Vertex[1] = tmpValue
    return

def main():
    # Checks for the right parameters
    #if len(sys.argv) < 3:
    #    print ("Usage: ConvertToOBJ.py Input.json ScaleFactor")
    #    return
    #if sys.argv[1].find(".json") < 0:
    #    print ("Error: Extension missing!")
    #    return

    # Stores the output filename (File.json -> File.obj)
    root = Tk()
    root.filename =  filedialog.askopenfilename(defaultextension=".json",
                                               filetypes=[('JSON files', '.json')],
                                               title="Choose TexturePacker json file")
    #OutputFilename = sys.argv[1][:-5] + ".obj"
    #OutMatFilename = sys.argv[1][:-5] + ".mtl"
    OutMatFilename = root.filename[:-5] + ".mtl"
    #ScaleFactor = float(sys.argv[2])
    ScaleFactor = 0.1
        
    # Everything is fine, Open the json file
    #file = open(sys.argv[1])
    file = open(root.filename, "r")
    Data = json.loads(file.read())

    # Close file
    file.close()

    #################################
    # Start Procesing the json data
    #################################
    
    #Metadata
    Metadata = Data["meta"]
    Alto = Metadata["size"]["h"]
    Ancho = Metadata["size"]["w"]

    # Output File
    #OutputFile = open(OutputFilename, "w+")

    # IndexCache
    #FaceIndexOffset = 0

    # Material
    OutMatFile = open(OutMatFilename, "w+")
    OutMatFile.write("newmtl TextureMap\nmap_Ka " + Metadata["image"] + "\nmap_Kd " + Metadata["image"])
    OutMatFile.close()

    # Frames
    Frames = Data["frames"]
    for FrameData in Frames:
        # DataFrame
        try:
            pivot = FrameData["pivot"]
        except :
            pivot = json.loads("{\"x\":0.5,\"y\":0.5}")

        sourceSize = FrameData["sourceSize"]
        rotated = FrameData["rotated"]
        OffsetSpriteWidth = sourceSize["w"] * pivot["x"]
        OffsetSpriteHeigth = sourceSize["h"] * pivot["y"]

        # Output File
        OutputFilename = FrameData["filename"][:-4] + ".obj"
        OutputFile = open(OutputFilename, "w+")

        OutputFile.write("mtllib " + OutMatFilename + "\n")
        OutputFile.write("o " + FrameData["filename"] + "\n")
        OutputFile.write("usemtl TextureMap\n")
        #print("o " + FrameData["filename"])

        for VertexData in FrameData["vertices"]:
            # Append an aditional value to the array
            VertexData.append(0)
            # Moves the Y value to Z (because UE4)
            VertexData[2] = VertexData[1]
            VertexData[1] = 0

            VertexData[0] = (VertexData[0] - OffsetSpriteWidth) * ScaleFactor
            VertexData[2] = (VertexData[2] - OffsetSpriteHeigth) * ScaleFactor

            RotateVertexXY(VertexData)
            RotateVertexXY(VertexData)

            # Frame Rotated?
            if rotated:
                RotateVertexXY(VertexData)

            # Write Vertex Data
            OutputFile.write("v " + str(-VertexData[0]) + " " + str(VertexData[1]) + " " + str(VertexData[2]) + "\n")
            #print("v " + str(VertexData[0]) + " " + str(VertexData[1]) + " " + str(VertexData[2]))

        for VertexUVData in FrameData["vertices"]:
            # Write Vertex Normal Data
            OutputFile.write("vn 0 -1 0\n")
            #print("v " + str(VertexData[0]) + " " + str(VertexData[1]) + " " + str(VertexData[2]))

        for VertexUVData in FrameData["verticesUV"]:
            VertexUVData[0] /= Ancho
            VertexUVData[1] /= Alto

            # Write VertexUV Data
            OutputFile.write("vt " + str(VertexUVData[0]) + " " + str(1-VertexUVData[1]) + "\n")
            #print("vt " + str(VertexUVData[0]) + " " + str(VertexUVData[1]))

        OutputFile.write("s off\n")
        #print("s off")

        for FaceData in reversed(FrameData["triangles"]):
            OutputFile.write("f " + str(FaceData[0] +1) + "/" + str(FaceData[0] +1) + "/" + " " + str(FaceData[1] +1) + "/" + str(FaceData[1] +1) + "/" + " " + str(FaceData[2] +1) + "/" + str(FaceData[2] +1) + "/" + "\n")
            #OutputFile.write("f " + str(FaceData[0] +1) + "/" + str(FaceData[0] +1) + " " + str(FaceData[1] +1) + "/" + str(FaceData[1] +1) + " " + str(FaceData[2] +1) + "/" + str(FaceData[2] +1) + "\n")
            #print("f " + str(FaceData[0] +1) + " " + str(FaceData[1] +1) + " " + str(FaceData[2] +1))

        #FaceIndexOffset += len(FrameData["vertices"])
        #print (FrameData["filename"], OffsetSpriteWidth, OffsetSpriteHeigth)

        OutputFile.close()

#end main()


if __name__ == "__main__":
    main()